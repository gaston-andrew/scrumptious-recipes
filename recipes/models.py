from django.db import models

# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    image = models.URLField(null=True, blank= True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, null= True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return float(self.name + "(" + self.abbreviation + ")")

class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

class Ingredient(models.Model):
    amount = models.FloatField(null=True, blank=True)
    recipe = models.ForeignKey(Recipe, related_name="ingredients_rqd", on_delete=models.CASCADE)
    measure = models.ForeignKey(Measure, related_name="amount", on_delete=models.PROTECT)
    food = models.ForeignKey(FoodItem,related_name="ingredient", on_delete=models.PROTECT)

    def __str__(self):
        return self.amount + " " + self.measure + " - " + self.food

class Step (models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)
    order = models.SmallIntegerField
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField(Recipe, related_name="tags")

    def __str__(self):
        return self.name
